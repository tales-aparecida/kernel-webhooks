"""Test sprinter webhook."""
import unittest
from unittest import mock

import gitlab

from webhook import sprinter


class TestSprinterHelpers(unittest.TestCase):
    """Test sprinter webhook helpers."""

    def test_get_label_name(self):
        """Test get_label_name function."""
        cases = [
            # First monday of year: 04/01
            ('2021-01-04T00:00:00.000Z', 'sprint::2021-week-1'),
            # First monday of year: 03/01
            ('2022-01-03T00:00:00.000Z', 'sprint::2022-week-1'),
            ('2022-01-10T00:00:00.000Z', 'sprint::2022-week-2'),
            ('2022-02-02T00:00:00.000Z', 'sprint::2022-week-5'),
        ]

        for date, label in cases:
            self.assertEqual(
                sprinter.get_label_name(date), label, (date, label)
            )

    @staticmethod
    @mock.patch('webhook.sprinter.LABELS_NAMESPACE', 'namespace')
    def test_create_group_label():
        """Test create_group_label function."""
        gl_instance = mock.Mock()

        sprinter.create_group_label(gl_instance, 'label::name')

        gl_instance.assert_has_calls([
            mock.call.groups.get('namespace', lazy=True),
            mock.call.groups.get().labels.create(
                {'name': 'label::name', 'color': '#AAAAAA'}
            )
        ])

    @mock.patch('webhook.sprinter.common.generic_loop')
    def test_main(self, mock_loop):
        """Test main entrypoint."""
        sprinter.main({})
        mock_loop.assert_called_with(mock.ANY, sprinter.WEBHOOKS)
        self.assertTrue(mock_loop.call_args[0][0].disable_inactive_branch_check)
        self.assertTrue(mock_loop.call_args[0][0].disable_closed_status_check)
        self.assertEqual(
            sprinter.WEBHOOKS,
            {'issue': sprinter.process_issue}

        )


class TestProcessIssue(unittest.TestCase):
    """Test sprinter webhook."""

    @staticmethod
    def test_not_interesting_message():
        """Test message we don't care about are ignored."""
        cases = [
            'open', 'merge', 'foobar'
        ]
        for case in cases:
            sprinter.process_issue(None, mock.Mock(payload={'object_attributes': {'action': case}}))

    @mock.patch('webhook.sprinter.gitlab.parse_gitlab_url')
    def test_reopen(self, mock_parse_url):
        """Test reopened issues get the labels removed."""
        message = {
            'object_attributes': {
                'action': 'reopen',
                'url': 'http://issue',
            }
        }

        issue = mock.Mock(labels=['foo', 'bar', 'sprint::2022-week-1'])
        mock_parse_url.return_value = None, issue

        sprinter.process_issue(None, mock.Mock(payload=message))

        self.assertEqual(['foo', 'bar'], issue.labels)
        self.assertTrue(issue.save.called)

    @mock.patch('webhook.sprinter.create_group_label')
    @mock.patch('webhook.sprinter.gitlab.parse_gitlab_url')
    def test_close(self, mock_parse_url, mock_create_label):
        """Test closed issues get the label added."""
        message = {
            'object_attributes': {
                'action': 'close',
                'url': 'http://issue',
                'closed_at': '2022-01-07 16:11:56 UTC',
            }
        }

        gl_instance = mock.Mock()
        issue = mock.Mock(labels=['foo', 'bar'])
        mock_parse_url.return_value = gl_instance, issue

        sprinter.process_issue(None, mock.Mock(payload=message))

        mock_create_label.assert_called_with(gl_instance, 'sprint::2022-week-1')

        self.assertEqual(['foo', 'bar', 'sprint::2022-week-1'], issue.labels)
        self.assertTrue(issue.save.called)

    @mock.patch('webhook.sprinter.create_group_label')
    @mock.patch('webhook.sprinter.gitlab.parse_gitlab_url')
    def test_close_label_exists(self, mock_parse_url, mock_create_label):
        """
        Test closed issues get the label added.

        When we try to create a label that already exists, Gitlab raises
        GitlabCreateError. Ensure it's gracefully handled.
        """
        message = {
            'object_attributes': {
                'action': 'close',
                'url': 'http://issue',
                'closed_at': '2022-01-07 16:11:56 UTC',
            }
        }

        mock_create_label.side_effect = gitlab.exceptions.GitlabCreateError

        gl_instance = mock.Mock()
        issue = mock.Mock(labels=['foo', 'bar'])
        mock_parse_url.return_value = gl_instance, issue

        sprinter.process_issue(None, mock.Mock(payload=message))

        mock_create_label.assert_called_with(gl_instance, 'sprint::2022-week-1')

        self.assertEqual(['foo', 'bar', 'sprint::2022-week-1'], issue.labels)
        self.assertTrue(issue.save.called)
