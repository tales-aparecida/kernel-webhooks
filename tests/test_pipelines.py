"""Webhook interaction tests."""
from unittest import TestCase

from webhook import defs
from webhook import pipelines


class TestPipelineType(TestCase):
    """Tests for the PipelineType enum."""

    PIPE_MAP = {'rhel9_private_merge_request': pipelines.PipelineType.REGULAR,
                'c9s_merge_request': pipelines.PipelineType.REGULAR,
                'c9s_realtime_merge_request': pipelines.PipelineType.REALTIME,
                'c9s_automotive_merge_request': pipelines.PipelineType.AUTOMOTIVE,
                'c9s_rhel9_compat_merge_request': pipelines.PipelineType.SHADOW
                }

    def _test_pipeline(self, func, pipe_name, result):
        result_string = result.name if isinstance(result, pipelines.PipelineType) else result
        print(f"Testing {func.__name__}() with '{pipe_name}', expecting '{result_string}'...")
        self.assertIs(func(pipe_name), result)

    def test_pipelinetype_from_str(self):
        """Returns the expected PipelineType value for the given input."""
        func = pipelines.PipelineType.from_str
        self._test_pipeline(func, '', pipelines.PipelineType.INVALID)
        self._test_pipeline(func, 'what', pipelines.PipelineType.INVALID)
        self._test_pipeline(func, 'regular', pipelines.PipelineType.REGULAR)
        self._test_pipeline(func, 'REALTIME', pipelines.PipelineType.REALTIME)
        self._test_pipeline(func, 'shadow', pipelines.PipelineType.SHADOW)
        self._test_pipeline(func, 'Automotive', pipelines.PipelineType.AUTOMOTIVE)
        for pipe_name, pipe_type in self.PIPE_MAP.items():
            self._test_pipeline(func, pipe_name, pipe_type)

    def test_pipeline_prefix(self):
        """Returns the expected CKI label prefix string."""
        self.assertEqual(pipelines.PipelineType.REGULAR.prefix, defs.CKI_KERNEL_PREFIX)
        self.assertEqual(pipelines.PipelineType.SHADOW.prefix, defs.CKI_KERNEL_PREFIX)
        self.assertEqual(pipelines.PipelineType.REALTIME.prefix, defs.CKI_KERNEL_RT_PREFIX)
        self.assertEqual(pipelines.PipelineType.AUTOMOTIVE.prefix,
                         defs.CKI_KERNEL_AUTOMOTIVE_PREFIX)
        self.assertIs(pipelines.PipelineType.INVALID.prefix, None)
