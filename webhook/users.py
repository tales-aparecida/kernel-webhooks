"""Library to represent a GL user."""
from collections import UserDict
import dataclasses

from cki_lib.logger import get_logger

LOGGER = get_logger('cki.webhook.users')


@dataclasses.dataclass(kw_only=True)
class User:
    """A GL user."""

    emails: list = dataclasses.field(default_factory=list)
    gid: str = ''
    name: str = ''
    username: str = ''
    user_dict: dataclasses.InitVar[dict] = {}

    def __eq__(self, other):
        """Return True if the usernames are equal, otherwise false."""
        return self.username == other.username

    def __post_init__(self, user_dict):
        """Fix up the username as needed."""
        if user_dict:
            self.update(user_dict)
        if not self.username:
            raise ValueError('username must be set.')
        LOGGER.debug('Created %s', self)

    def __repr__(self):
        """Say who you are."""
        return f"<User '{self.username}', id: {self.id}>"

    def add_email(self, email):
        """Append an email if it doesn't already exist."""
        if email and email not in self.emails:
            self.emails.append(email)

    @property
    def id(self):
        # pylint: disable=invalid-name
        """Return the id from the gid as an int."""
        return int(self.gid.rsplit('/', 1)[-1]) if self.gid else 0

    def update(self, user_dict):
        """Update ourself from the input dict."""
        dict_copy = user_dict.copy()
        # owners.yaml Entry dicts use gluser instead of username.
        if 'username' not in dict_copy and 'gluser' in dict_copy:
            dict_copy['username'] = dict_copy.pop('gluser')
        # If the username in the dict doesn't match this object's existing username then boom.
        if self.username and self.username != dict_copy['username']:
            raise ValueError(('The dict username and object username do not match: '
                             f"{dict_copy['username']}, {self.username}'"))
        for key in {field.name for field in dataclasses.fields(self.__class__) if field.init}:
            if key in dict_copy:
                setattr(self, key, dict_copy[key])
        self.add_email(dict_copy.get('email', ''))


class UserCache(UserDict):
    """A cache for users."""

    def __init__(self, graphql):
        """Say hello."""
        super().__init__()
        self.graphql = graphql
        LOGGER.info('Created %s', self)

    def __repr__(self):
        """Represent myself."""
        return f'<UserCache with {len(self.data)} users>'

    def _find_user(self, namespace, attribute, search_key):
        """Return the found User or None."""
        if user_data := self.graphql.find_member(namespace, attribute, search_key):
            return self.new_user(user_dict=user_data)
        return None

    def get_by_email(self, email, namespace=None):
        """Return the matching User if any, or None."""
        user = next((user for user in self.data.values() if email in user.emails), None)
        if not user and namespace:
            if user := self._find_user(namespace, 'email', email):
                user.add_email(email)
        return user

    def get_by_id(self, userid, namespace=None):
        """Return the matching User if any, or None."""
        user = next((user for user in self.data.values() if user.id == int(userid)), None)
        if not user and namespace:
            # This way we only "get" the user if they are part of the given namespace.
            if user := self.graphql.get_user_by_id(f'gid://gitlab/User/{userid}'):
                return self.get_by_username(user['username'], namespace)
        return user

    def get_by_username(self, username, namespace=None):
        """Return the matching User if any, or None."""
        user = self.data.get(username)
        if not user and namespace:
            user = self._find_user(namespace, 'username', username)
        return user

    def new_user(self, user_dict):
        """Create or update the User in the cache and then return it."""
        username = user_dict.get('username') or user_dict.get('gluser')
        if username in self.data:
            self.data[username].update(user_dict)
        else:
            self.data[username] = User(user_dict=user_dict)
        return self.data[username]

    def user_has_email(self, user, email, namespace):
        """Return True if the user is found to have the given email, otherwise False."""
        if result := self.graphql.find_member_by_email(namespace, email, user.username):
            user = self.new_user(user_dict=result)
            user.add_email(email)
            return True
        return False
