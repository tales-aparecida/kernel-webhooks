"""Library to consistently parse an MR or commit description."""
from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
import re

from cki_lib.logger import get_logger
from unidecode import unidecode

from webhook.defs import DCOState
from webhook.defs import EXT_TYPE_URL
from webhook.defs import JPFX
from webhook.libbz import fetch_bugs
from webhook.users import User

LOGGER = get_logger('cki.webhook.description')

BUGZILLA_TAG = 'Bugzilla'
JIRA_ISSUE_TAG = 'JIRA'
DEPENDS_TAG = 'Depends'


@dataclass
class Description:
    """Parse text tags into sets of IDs."""

    text: str = field(default='', repr=False)
    namespace: str = ''                         # Optional namespace to match Depends: MR ID tags
    _depends: set = field(default_factory=set, init=False)  # Hack to store more Depends: BZ IDs

    def __bool__(self):
        """Return True if self.text is not empty, otherwise False."""
        return bool(self.text)

    def __eq__(self, other):
        """Return True if the text, namespace, and _depends attribute values are the same."""
        return self.text == other.text and self.namespace == other.namespace and \
            self._depends == other._depends

    @staticmethod
    def _parse_tag(tag_prefix, text):
        """Return the set of tag IDs as ints."""
        # tag_prefix: http://bugzilla.redhat.com/1234567
        # tag_prefix: https://bugzilla.redhat.com/show_bug.cgi?id=1234567
        pattern = r'^' + tag_prefix + \
            r': https?://bugzilla\.redhat\.com/(?:show_bug\.cgi\?id=)?(\d{4,8})\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return {int(tag) for tag in tag_regex.findall(text)}

    @staticmethod
    def _parse_jira_tag(tag_prefix, text):
        """Return the set of tags as strs."""
        # tag_prefix: https://issues.redhat.com/browse/RHEL-1
        # tag_prefix: https://issues.redhat.com/projects/RHEL/issues/RHEL-1
        pattern = r'^' + tag_prefix + \
            r': https://issues\.redhat\.com/(?:browse|projects/RHEL/issues)/(' + \
            JPFX + r'\d{1,8})\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return {str(tag) for tag in tag_regex.findall(text)}

    @property
    def bugzilla(self):
        """Return the set of BZ IDs parsed from any Bugzilla: tags."""
        return self._parse_tag(BUGZILLA_TAG, self.text)

    @property
    def jissue(self):
        """Return the set of JIssue IDs parsed from any JIRA: tags."""
        return self._parse_jira_tag(JIRA_ISSUE_TAG, self.text)

    @property
    def cve(self):
        """Return the set of CVE IDs parsed from CVE: tags."""
        pattern = r'^CVE: (CVE-\d{4}-\d{4,7})\s*$'
        cve_regex = re.compile(pattern, re.MULTILINE)
        return set(cve_regex.findall(self.text))

    @property
    def depends(self):
        """Return the set of BZ IDs parsed from any Depends: tags plus any stashed in _depends."""
        return self._parse_tag(DEPENDS_TAG, self.text) | self._depends

    def add_to_depends(self, mr_ids):
        """Add the mr_id list to the _depends set that is appended to the depends set."""
        self._depends.update(mr_ids)

    @property
    def depends_mrs(self):
        """Return the set of MR IDs parsed from any Depends: tags."""
        # Depends: https://gitlab.com/group/subgroup/project/-/merge_requests/123
        # Depends: !123
        dep_mrs = set()
        if self.namespace:
            pattern = r'^' + DEPENDS_TAG + \
                r': (?:https?://gitlab\.com/' + self.namespace + r'/-/merge_requests/|!)(\d+)\s*$'
        else:
            pattern = r'^' + DEPENDS_TAG + r': !(\d+)\s*$'
        mr_regex = re.compile(pattern, re.MULTILINE)
        dep_mrs.update([int(bz_id) for bz_id in mr_regex.findall(self.text)])

        # If we have a namespace and there are Depends BZs then try to find their MR IDs.
        if self.namespace:
            if dep_bzs := self._parse_tag(DEPENDS_TAG, self.text):
                for bz_bug in fetch_bugs(dep_bzs):
                    if mr_ids := self._get_bz_mr_id(bz_bug.external_bugs, self.namespace):
                        LOGGER.info('Bug %s resolved to MR(s) %s', bz_bug.id, mr_ids)
                        dep_mrs.update(mr_ids)
                    else:
                        LOGGER.info('Bug %s not linked to any MR in the %s namespace.', bz_bug.id,
                                    self.namespace)
        return dep_mrs

    @property
    def marked_internal(self):
        """Return True if the text has Bugzilla|JIRA: INTERNAL, otherwise False."""
        pattern = r'^(' + BUGZILLA_TAG + '|' + JIRA_ISSUE_TAG + r'): INTERNAL\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return bool(tag_regex.findall(self.text))

    @property
    def signoff(self):
        """Return the set of valid DCO Signed-off-by tags."""
        # Each item in the set is a tuple with the name and email.
        # There is no validation of the email string.
        pattern = r'^Signed-off-by: (.+) <(.+)>\s*$'
        dco_regex = re.compile(pattern, re.MULTILINE)
        return set(dco_regex.findall(self.text))

    @staticmethod
    def _get_bz_mr_id(external_bugs, namespace):
        """Return the list of MR IDs in the BZs external trackers that match the namespace."""
        return {int(eb['ext_bz_bug_id'].split('/')[-1]) for eb in external_bugs if
                eb['type']['url'] == EXT_TYPE_URL and
                eb['ext_bz_bug_id'].startswith(f'{namespace}/-/merge_requests/')}


@dataclass(kw_only=True)
class Commit:
    """Properties of a commit."""

    author: User | None = field(default=None)
    authorEmail: str = ''  # pylint: disable=invalid-name
    authorName: str = ''  # pylint: disable=invalid-name
    date: datetime | None = field(default=None)
    description: Description = field(default_factory=Description)
    sha: str = ''
    title: str = ''
    input_dict: InitVar[dict] = {}
    namespace: InitVar[str] = ''

    def __post_init__(self, input_dict, namespace):
        """Set it up from the input_dict."""
        if input_dict:
            if author_dict := input_dict.get('author'):
                if self.author:
                    raise ValueError('input_dict has author key but author attrib is already set.')
                self.author = User(user_dict=author_dict)
            self.authorEmail = input_dict.get('authorEmail', '')
            self.authorName = input_dict.get('authorName', '')
            self.date = datetime.fromisoformat(input_dict['authoredDate'][:19]) if \
                'authoredDate' in input_dict else None
            self.description = Description(text=input_dict.get('description', ''),
                                           namespace=namespace)
            self.sha = input_dict.get('sha', '')
            self.title = input_dict.get('title', '')
        # If GL has associated this commit with this User then we can assume the commit
        # authorEmail belongs to this User.
        if self.author and self.authorEmail:
            self.author.add_email(self.authorEmail)
        LOGGER.debug('Created %s', self)

    def __repr__(self):
        """Display it."""
        repr_str = f"{self.short_sha} ('{self.title}')"
        author = self.author or f'{self.authorName} ({self.authorEmail})'
        return f'<Commit {repr_str}, author: {author}, dco: {self.dco_state.name}>'

    @property
    def alternate_signoffs(self):
        """Return a list of alternate valid signoffs if the author has multiple emails."""
        if not self.author:
            return []
        return [(self.authorName, email) for email in self.author.emails if
                email != self.authorEmail]

    @property
    def dco_state(self):
        """Return the DCOState of the Commit."""
        # None of the known emails for this author are @redhat.com so :/
        all_emails = self.author.emails if self.author else [self.authorEmail]
        if not any(email for email in all_emails if email.endswith('@redhat.com')):
            return DCOState.NOT_REDHAT
        # There were no signoffs found in the commit description.
        if not self.description.signoff:
            return DCOState.MISSING
        # At least one of the expected/alternate signoffs matches the Description.signoff and has a
        # @redhat.com email address.
        description_signoffs = [(unidecode(name), mail) for name, mail in self.description.signoff]
        possible_signoffs = [(unidecode(name), mail) for name, mail in
                             self.alternate_signoffs + [self.expected_signoff]]
        if any(signoff in description_signoffs for signoff in possible_signoffs if
               signoff[1].endswith('@redhat.com')):
            return DCOState.OK
        state = DCOState.UNRECOGNIZED
        # Do any of the commit DCOs have a name that matches the commit authorName?
        if any(dco for dco in description_signoffs if dco[0] == unidecode(self.authorName)):
            state = DCOState.NAME_MATCHES
        # ... or do any of the commit DCOs have an email address that matches the authorEmail?
        elif any(dco for dco in description_signoffs if dco[1] in all_emails):
            state = DCOState.EMAIL_MATCHES
        return state

    @property
    def expected_signoff(self):
        """Return a tuple representing the expected DCO signoff if authorName/Email set, or None."""
        if self.authorEmail and self.authorName:
            return (self.authorName, self.authorEmail)
        return None

    @property
    def short_sha(self):
        """Return the first 12 chars of the sha."""
        return self.sha[:12]
