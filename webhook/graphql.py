"""Common helpers for graphql."""
from datetime import datetime
import functools

from cki_lib import gitlab
from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from cki_lib.misc import is_production

from webhook import fragments
from webhook.defs import GITFORGE

LOGGER = get_logger('cki.webhook.graphql')

GET_USER_DETAILS_BASE = """
query userData {
  ...CurrentUser
}
"""

GET_USER_DETAILS_QUERY = GET_USER_DETAILS_BASE + fragments.CURRENT_USER

# Query for GitlabGraph.create_note()
CREATE_NOTE_MUTATION = """
mutation createNote($globalId: NoteableID!, $body: String!) {
  createNote(input: {noteableId: $globalId, body: $body}) {
    note {
      id
    }
  }
}
"""

UPDATE_NOTE_MUTATION = """
mutation updateNote($globalId: NoteID!, $body: String!) {
  updateNote(input: {id: $globalId, body: $body}) {
    note {
      id
    }
  }
}
"""

FIND_NOTE_QUERY = """
query mrData($namespace: ID!, $mr_id: String!) {
  project(fullPath: $namespace) {
    mergeRequest(iid: $mr_id) {
      id
      discussions {
        nodes {
          notes {
            nodes {
              author {
                username
              }
              body
              id
            }
          }
        }
      }
    }
  }
}
"""

FIND_MEMBER_BASE = """
query mrData($namespace: ID!, $search_key: String!) {
  group(fullPath: $namespace) {
    groupMembers(search: $search_key) {
      nodes {
        user {
          ...GlUser
        }
      }
    }
  }
  project(fullPath: $namespace) {
    projectMembers(search: $search_key) {
      nodes {
        user {
          ...GlUser
        }
      }
    }
  }
}
"""

FIND_MEMBER_QUERY = FIND_MEMBER_BASE + fragments.GL_USER

GET_USER_BY_ID_BASE = """
query mrData($userid: UserID!) {
  user(id: $userid) {
    ...GlUser
  }
}
"""

GET_USER_BY_ID_QUERY = GET_USER_BY_ID_BASE + fragments.GL_USER


class GitlabGraph:
    """A wrapper object for interacting with gitlab graphql."""

    @staticmethod
    def _check_user(results, check_user):
        """Return True if the query currentUser username matches the check_user."""
        return check_user == results['currentUser']['username']

    @staticmethod
    def _check_keys(results, check_keys):
        """Return True if all the keys are in the given query results."""
        return check_keys <= results.keys()

    @classmethod
    def check_query_results(cls, results, check_keys=None, check_user=None):
        """Perform some optional checks of query results."""
        # See GitlabGraph.execute_query().
        # Ignore our own messages (for bots). Query must include 'currentUser' field.
        if check_user and cls._check_user(results, check_user):
            LOGGER.info('Ignoring message from %s.', check_user)
            return None

        # Raise an error if not all expected keys are in the results.
        if check_keys and not cls._check_keys(results, check_keys):
            raise RuntimeError(f'Gitlab did not return all keys {check_keys} in {results}.')
        return results

    def __init__(self, get_user=False):
        """Set up the client."""
        self.client = gitlab.get_graphql_client(GITFORGE)
        LOGGER.info('Connected.')
        if get_user:
            LOGGER.info('Logged in as %s (%s).', self.username, self.user_id)

    @functools.cached_property
    def user(self):
        """Return the details of the user we are logged in as."""
        return self.client.query(GET_USER_DETAILS_QUERY)['currentUser']

    @property
    def user_id(self):
        """Return the user ID of the user we are logged in as."""
        return int(self.user['id'].rsplit('/')[-1])

    @property
    def username(self):
        """Return the username we are logged in as."""
        return self.user['username']

    def get_user_by_id(self, userid):
        """Return the user with the given GID, or None."""
        if isinstance(userid, int):
            userid = f'gid://gitlab/User/{userid}'
        if not isinstance(userid, str) or not userid.startswith('gid://gitlab/User/'):
            raise ValueError('userid must be an int or valid GID string.')
        results = self.client.query(GET_USER_BY_ID_QUERY, {'userid': userid})
        return results.get('user') if results else None

    def find_member(self, namespace, attribute, search_key):
        """Find the member of the given namespace with the matching attribute."""
        # This won't find anything if the search_key isn't in the first 100 results!
        if not attribute or not search_key:
            raise ValueError('attibute and search_key must be non-zero length strings.')
        params = {'namespace': namespace, 'search_key': search_key}
        results = self.client.query(FIND_MEMBER_QUERY, params)
        users = get_nested_key(results, 'group/groupMembers/nodes') or \
            get_nested_key(results, 'project/projectMembers/nodes')
        return next((user['user'] for user in users if
                     user['user'].get(attribute, None) == search_key), None)

    def find_member_by_email(self, namespace, email, username):
        """Find the member with the given email that has the given username."""
        # This won't find anything if the search_key isn't in the first 100 results!
        if not email or not username:
            raise ValueError('email and username must be non-zero length strings.')
        params = {'namespace': namespace, 'search_key': email}
        results = self.client.query(FIND_MEMBER_QUERY, params)
        users = get_nested_key(results, 'group/groupMembers/nodes') or \
            get_nested_key(results, 'project/projectMembers/nodes')
        return next((user['user'] for user in users if
                     user['user']['username'] == username), None)

    def _do_note(self, action, global_id, body):
        """Handle creating or updating a note."""
        params = {'globalId': global_id, 'body': body}
        if action == 'create':
            result = self.check_query_results(self.client.query(
                CREATE_NOTE_MUTATION, params), check_keys={'createNote'})
        elif action == 'update':
            result = self.check_query_results(self.client.query(
                UPDATE_NOTE_MUTATION, params), check_keys={'updateNote'})
        if not result:
            raise RuntimeError('Note mutation did not return expected results.')
        return result

    def create_note(self, global_id, body):
        """Create a note if in prod. Returns the new note's global ID on success, otherwise None."""
        LOGGER.info('Creating new note for %s:\n%s', global_id, body)
        if not is_production():
            return True
        result = self._do_note('create', global_id, body)
        return result['createNote']['note']['id']  # pylint: disable=unsubscriptable-object

    def update_note(self, global_id, body):
        """Update an existing note. Returns the new note's global ID on success, otherwise None."""
        body += f'\n\nLast updated at {datetime.now().isoformat()[:19]}.'
        LOGGER.info('Updating note on %s:\n%s', global_id, body)
        if not is_production():
            return True
        result = self._do_note('update', global_id, body)
        return result['updateNote']['note']['id']  # pylint: disable=unsubscriptable-object

    @staticmethod
    def _search_discussion(discussion, username, substring):
        """Find the note in the discussion by the username with matching substring, if any."""
        for note in discussion['notes']['nodes']:
            if note['author']['username'] == username and substring in note['body']:
                return note['id']
        return None

    def replace_note(self, namespace, mr_id, username, substring, body):
        # pylint: disable=too-many-arguments,unsubscriptable-object
        """
        Replace an existing note with body.

        Searches the MR discussions for a note by a matching username that contains the substring
        and if found, replaces it with body. If not, a new note is created.
        Operates on the first matching note.
        """
        params = {'namespace': namespace, 'mr_id': str(mr_id)}
        if not (result := self.check_query_results(self.client.query(FIND_NOTE_QUERY, params),
                                                   check_keys={'project'})):
            raise RuntimeError('Find note query did not return expected results.')
        note_id = None
        for discussion in result['project']['mergeRequest']['discussions']['nodes']:
            if note_id := self._search_discussion(discussion, username, substring):
                LOGGER.debug('Found existing note %s.', note_id)
                break
        if note_id:
            return self.update_note(note_id, body)
        return self.create_note(result['project']['mergeRequest']['id'], body)
