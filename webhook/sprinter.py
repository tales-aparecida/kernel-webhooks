"""Label issues when they are closed."""
import os
import sys

from cki_lib import gitlab
from cki_lib import logger
from cki_lib import misc
import dateutil.parser

from . import common

LOGGER = logger.get_logger('cki.webhook.sprinter')

LABELS_NAMESPACE = os.environ.get('LABELS_NAMESPACE', 'cki-project')
LABELS_FORMAT = 'sprint::{year}-week-{week}'


def get_label_name(date):
    """Return label name for current sprint."""
    date = dateutil.parser.parse(date).isocalendar()
    return LABELS_FORMAT.format(year=date.year, week=date.week)


def create_group_label(gl_instance, name, color='#AAAAAA'):
    """Create a group label with the given attributes."""
    gl_instance.groups.get(LABELS_NAMESPACE, lazy=True).labels.create({
        'name': name,
        'color': color,
    })


def process_issue(_, message, **__):
    """Add sprint label to closed issues."""
    action = misc.get_nested_key(message.payload, 'object_attributes/action')
    if action not in ('close', 'reopen'):
        return

    issue_url = misc.get_nested_key(message.payload, 'object_attributes/url')
    instance, issue = gitlab.parse_gitlab_url(issue_url)

    if action == 'reopen':
        # Remove any sprint:: label it might have from before
        for label in issue.labels:
            if label.startswith('sprint::'):
                LOGGER.info('Removing %s label from: %s', label, issue_url)
                issue.labels.remove(label)
        issue.save()

    elif action == 'close':
        # Add label matching the current sprint
        label_name = get_label_name(misc.get_nested_key(
            message.payload, 'object_attributes/closed_at'))
        try:
            create_group_label(instance, label_name)
        except gitlab.gitlab.exceptions.GitlabCreateError:
            # Already exists
            pass

        LOGGER.info('Adding %s label to: %s', label_name, issue_url)
        issue.labels.append(label_name)
        issue.save()


WEBHOOKS = {
    "issue": process_issue,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SPRINTER')
    args = parser.parse_args(args)
    args.disable_inactive_branch_check = True
    args.disable_closed_status_check = True

    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
