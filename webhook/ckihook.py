"""Manage the CKI labels."""
from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from enum import IntEnum
from enum import auto
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.gitlab import get_variables
import prometheus_client as prometheus

from webhook import common
from webhook import defs
from webhook import fragments
from webhook.pipelines import PipelineType

LOGGER = logger.get_logger('cki.webhook.ckihook')


METRIC_KWF_CKIHOOK_PIPELINES_RETRIED = prometheus.Counter(
    'kwf_ckihook_pipelines_retried',
    'Number of CKI pipelines retried by ckihook',
    ['project_path']
)


class PipelineStatus(IntEnum):
    """Possible status of a pipeline."""

    UNKNOWN = auto()
    MISSING = auto()
    FAILED = auto()
    CANCELED = auto()
    RUNNING = auto()
    PENDING = RUNNING
    CREATED = RUNNING
    OK = auto()
    SUCCESS = OK

    def title(self):
        """Return capitalized name."""
        return self.name.capitalize() if self.name != 'OK' else 'OK'

    @classmethod
    def from_str(cls, status):
        """Return the enum value that matches the given status name."""
        return next((member for name, member in cls.__members__.items()
                     if name == status.upper()), cls.UNKNOWN)


@dataclass(frozen=True)
class PipelineResult:
    """Basic pipeline details."""

    api_dict: InitVar(dict) = {}
    id: int = field(init=False, default=0)  # pylint: disable=invalid-name
    name: str = field(init=False, default='')
    status: PipelineStatus = field(init=False, default=PipelineStatus.UNKNOWN)
    label: str = field(init=False, default='')
    type: PipelineType = field(init=False, default=PipelineType.INVALID)
    failed_stage: str = field(init=False, default='')
    project_id: int = field(init=False, default=0)

    def __post_init__(self, api_dict):
        """Parse the api_dict."""
        self.__dict__['id'] = int(api_dict['id'].rsplit('/', 1)[-1])
        self.__dict__['name'] = api_dict['sourceJob']['name']
        self.__dict__['type'] = PipelineType.from_str(self.name)
        self.__dict__['status'] = PipelineStatus.from_str(api_dict['status'])
        self.__dict__['project_id'] = int(api_dict['project']['id'].rsplit('/', 1)[-1])
        if self.status is PipelineStatus.FAILED:
            self.__dict__['failed_stage'] = self._get_failed_stage(api_dict['stages']['nodes'])
        # Only set a label for the result if the pipeline has a test stage. Otherwise who cares?
        if any(stage for stage in api_dict['stages']['nodes'] if stage['name'] == 'test') or \
           self.name == 'ark_merge_request':
            self._set_label()
        LOGGER.debug('Created %s', self)

    def _set_label(self):
        """Set the label for pipelines of a known `type`."""
        if not (prefix := self.type.prefix):
            return
        self.__dict__['label'] = f'{prefix}::{self.status.title()}::{self.failed_stage}' if \
                                 self.failed_stage else f'{prefix}::{self.status.title()}'

    @staticmethod
    def _get_failed_stage(stages):
        """Return the name of the latest failed stage for a Pipeline that has failed."""
        return next((stage['name'] for stage in reversed(stages) for
                     job in stage['jobs']['nodes'] if job['status'].upper() == 'FAILED'), '')


PIPELINE_QUERY_CHECK_KEYS = {'currentUser', 'project'}
PIPELINE_BASE = """
query mrData($namespace: ID!, $mr_id: String!) {
  ...CurrentUser
  project(fullPath: $namespace) {
    mr: mergeRequest(iid: $mr_id) {
      headPipeline {
        id
        status
        downstream {
          nodes {
            ...CkiPipeline
          }
        }
      }
    }
  }
}
"""

PIPELINE_QUERY = PIPELINE_BASE + fragments.CURRENT_USER + fragments.CKI_PIPELINE


def validate_cki_query_results(results):
    """Check PIPELINE_QUERY query results and return them."""
    if not results:
        LOGGER.info('Nothing to parse.')
        return []
    # The MR value will be None if it doesn´t exist.
    if not results['project'].get('mr'):
        LOGGER.warning('Merge request does not exist, ignoring.')
        return []
    # If the headPipeline or downstream data is not available then we have nothing to do.
    if not (head_pipeline := results['project']['mr'].get('headPipeline')):
        LOGGER.warning('MR does not have headPipeline set.')
        return []
    if not head_pipeline['downstream']['nodes']:
        LOGGER.warning('headPipeline does not have any downstream nodes set.')
        return []
    return results


OPEN_MRS_BASE = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mrs: mergeRequests(
      after: $after
      state: opened,
      labels: ["CKI_RT::Failed::merge"]
      targetBranches: $branches) {
      pageInfo {hasNextPage endCursor}
      nodes {
        iid
        ...MrLabels
        headPipeline {
          id
          status
          downstream {
            nodes {
              ...CkiPipeline
            }
          }
        }
      }
    }
  }
}
"""

OPEN_MRS_QUERY = OPEN_MRS_BASE + fragments.MR_LABELS + fragments.CKI_PIPELINE


def fetch_cki(graphql, namespace, mr_id):
    """Run the PIPELINE_QUERY and return the validated results dict."""
    LOGGER.info('Fetching pipeline data for %s!%s.', namespace, mr_id)
    query_params = {'namespace': namespace, 'mr_id': str(mr_id)}
    results = graphql.client.query(PIPELINE_QUERY, query_params)
    return validate_cki_query_results(results)


def map_pipeline_query_results(results, key='project/mr/headPipeline/downstream/nodes'):
    """Map query results into a list of PipelineResult objects using the given key."""
    ds_nodes = misc.get_nested_key(results, key, default=[])
    return [PipelineResult(api_dict=downstream_node) for downstream_node in ds_nodes]


def parse_mr_url(url):
    """Return MR namespace and ID parsed from a gitlab url."""
    mr_id = int(url.split('/')[-1])
    namespace = url.removeprefix(f'{defs.GITFORGE}/').removesuffix(f'/-/merge_requests/{mr_id}')
    return namespace, mr_id


def add_labels(namespace, mr_id, labels):
    """Set the given labels."""
    LOGGER.info('Setting labels %s', labels)
    gl_instance = get_instance(defs.GITFORGE)
    gl_project = gl_instance.projects.get(namespace)
    common.add_label_to_merge_request(gl_project, mr_id, labels, remove_scoped=True)


def discard_waived_labels(mr_labels, labels_to_add, waived_labels):
    """Discard all labels marked as Waived."""
    for label in waived_labels:
        if f"{label}::Waived" in mr_labels:
            LOGGER.info('MR has %s::Waived, discarding new label.',
                        label)
            labels_to_add = [lbl for lbl in labels_to_add if not
                             lbl.startswith(f'{label}::')]

    return labels_to_add


def compute_new_labels(graphql, namespace, mr_id, pipeline_types):
    """Return a new list of CKI labels for the given MR."""
    results = fetch_cki(graphql, namespace, mr_id)
    pipeline_list = map_pipeline_query_results(results)
    new_labels = [pipe.label for pipe in pipeline_list if
                  pipe.type in pipeline_types and pipe.label]
    # Return our new labels but if any are missing append ::Missing ones as well
    return new_labels + check_for_missing_labels(new_labels, pipeline_types) if new_labels else []


def cki_label_changed(changes):
    """Return True if any CKI label changed, or False."""
    return (common.has_label_prefix_changed(changes, f'{defs.CKI_KERNEL_PREFIX}::') or
            common.has_label_prefix_changed(changes, f'{defs.CKI_KERNEL_RT_PREFIX}::') or
            common.has_label_prefix_changed(changes, f'{defs.CKI_KERNEL_AUTOMOTIVE_PREFIX}::'))


def check_for_missing_labels(label_list, pipeline_types):
    """Return a list of Missing-scoped labels that are not in label_list for the given pipelines."""
    return [f'{pipe.prefix}::Missing' for pipe in pipeline_types if not
            any(label for label in label_list if label.startswith(f'{pipe.prefix}::'))]


def retry_pipelines(failed_pipelines):
    """Get a gitlab instance and retry the failed pipelines."""
    # Input is a dict with mr_id as key with a PipelineResult value.
    gl_instance = get_instance(defs.GITFORGE)
    gl_projects = {}
    for mr_id, pipeline in failed_pipelines.items():
        if not (gl_project := gl_projects.get(pipeline.project_id)):
            gl_project = gl_instance.projects.get(pipeline.project_id)
            gl_projects[gl_project.id] = gl_project
        gl_pipeline = gl_project.pipelines.get(pipeline.id)
        prepare_python_job_id = next((job.id for job in gl_pipeline.jobs.list(iterator=True) if
                                      job.name == 'prepare python' and
                                      job.status == 'success'), None)
        if not prepare_python_job_id:
            LOGGER.warning("Downstream pipeline %s does not have a 'prepare python' job to retry?")
            continue
        gl_job = gl_project.jobs.get(prepare_python_job_id)

        LOGGER.info('Retrying downstream pipeline #%s (job #%s) on %s for MR %s', gl_pipeline.id,
                    gl_job.id, gl_project.path_with_namespace, mr_id)
        if misc.is_production():
            gl_job.retry()
            gl_pipeline.retry()
            METRIC_KWF_CKIHOOK_PIPELINES_RETRIED.labels(gl_project.path_with_namespace).inc()


def failed_rt_mrs(graphql, namespace, branches):
    """Return a dict of namespace MRs open on the branches that failed CKI_RT merge."""
    query_params = {'namespace': namespace, 'branches': branches}
    result = graphql.client.query(OPEN_MRS_QUERY, query_params, paged_key='project/mrs')
    # The query only returns MRs with a CKI_RT::Failed::merge label so nothing to filter here.
    return {mr['iid']: mr['headPipeline']['downstream'] for
            mr in result['project']['mrs']['nodes'][:10]} if result.get('project') else {}


def retrigger_failed_pipelines(graphql, namespace, branch):
    """Retrigger any RT pipelines that failed on or before the MERGE stage."""
    if PipelineType.REALTIME not in branch.pipelines:
        LOGGER.info('This branch does not expect a realtime pipeline: %s', branch.pipelines)
        return
    branch_names = [branch.name, branch.name.removesuffix('-rt')]
    if not (mrs := failed_rt_mrs(graphql, namespace, branch_names)):
        LOGGER.info('No open MRs for %s on branches %s, nothing to do.', namespace, branch_names)
        return
    failed_pipes = {mr_id: pipe for mr_id, downstream_nodes in mrs.items() for pipe in
                    map_pipeline_query_results(downstream_nodes, key='nodes') if
                    pipe.label == 'CKI_RT::Failed::merge'}
    LOGGER.info('Found %s MRs and %s RT pipelines that failed on Merge for %s.', len(mrs),
                len(failed_pipes), namespace)
    retry_pipelines(failed_pipes)


def get_project_pipeline_var(gl_project, pipeline_id, key):
    """Return the value of the pipeline variable matching key, or None."""
    gl_pipeline = gl_project.pipelines.get(pipeline_id)
    return get_variables(gl_pipeline).get(key)


def get_pipeline_target_branch(gl_instance, project_id, pipeline_id):
    """Return the 'branch' pipeline variable value for the given project/pipeline."""
    gl_project = gl_instance.projects.get(project_id)
    return get_project_pipeline_var(gl_project, pipeline_id, 'branch')


def get_downstream_pipeline_branch(gl_instance, pipeline_data):
    """Return the 'branch' var value if found, otherwise None."""
    ds_pipeline = pipeline_data['project']['mr']['headPipeline']['downstream']['nodes'][0]
    ds_project_id = int(ds_pipeline['project']['id'].split('/')[-1])
    ds_pipeline_id = int(ds_pipeline['id'].split('/')[-1])

    if not (ds_branch := get_pipeline_target_branch(gl_instance, ds_project_id, ds_pipeline_id)):
        LOGGER.debug("Could not get 'branch' variable from downstream pipeline %s", ds_pipeline_id)
    return ds_branch


def process_possible_branch_change(graphql, namespace, mr_id, msg):
    """Confirm MR branch changed and if so, Return True and cancel/trigger pipelines."""
    LOGGER.info('Checking if MR target branch matches latest pipeline target branch.')
    # If the MR doesn't have a head pipeline then we're done here.
    if not msg.payload['object_attributes']['head_pipeline_id']:
        LOGGER.info('No head pipeline, skipping target branch check.')
        return False
    # This is the signature of an MR event when the target branch changes. Maybe?
    if 'merge_status' not in msg.payload['changes']:
        LOGGER.info(
            "'changes' dict does not have 'merge_status' key, skipping target branch check."
        )
        return False
    if pipeline_data := fetch_cki(graphql, namespace, mr_id):
        gl_instance = msg.gl_instance()
        ds_branch = get_downstream_pipeline_branch(gl_instance, pipeline_data)
        mr_branch = msg.payload['object_attributes']['target_branch']
        if ds_branch and ds_branch != mr_branch:
            LOGGER.info('Target branch changed: MR %s != downstream pipeline %s, triggering...',
                        mr_branch, ds_branch)
            if not misc.is_production():
                LOGGER.info('Not production, skipping work.')
                return True
            head_pipeline_id = msg.payload['object_attributes']['head_pipeline_id']
            gl_project = gl_instance.projects.get(namespace)
            gl_mr = gl_project.mergerequests.get(mr_id)
            common.cancel_pipeline(gl_project, head_pipeline_id)
            new_pipeline_id = common.create_mr_pipeline(gl_mr)
            new_pipeline_url = f'{gl_project.web_url}/-/pipelines/{new_pipeline_id}'
            note_text = ("This MR's current target branch has changed since the last pipeline"
                         f" was triggered. The last pipeline {head_pipeline_id} has been canceled"
                         f" and a new pipeline has been triggered: {new_pipeline_url}  \n"
                         f"Last pipeline MR target branch: {ds_branch}  \n"
                         f"Current MR target branch: {mr_branch}  ")
            common.create_note(gl_mr, note_text)
            return True
    LOGGER.info("MR current target branch '%s' matches latest pipeline.", mr_branch)
    return False


def process_mr_event(msg, projects, graphql, **_):
    """Process an MR message."""
    mr_id = msg.payload['object_attributes']['iid']
    mr_labels = [label['title'] for label in msg.payload['labels']]
    namespace = msg.payload['project']['path_with_namespace']
    LOGGER.info('Processing MR event for %s!%s.', namespace, mr_id)
    if process_possible_branch_change(graphql, namespace, mr_id, msg):
        return

    branch = projects.get_target_branch(msg.payload['project']['id'],
                                        msg.payload['object_attributes']['target_branch'])

    # If someone changed the CKI labels then recalculate them.
    if cki_label_changed(msg.payload['changes']):
        LOGGER.info('CKI labels changed. Calculating new ones.')
        if labels_to_add := compute_new_labels(graphql, namespace, mr_id, branch.pipelines):
            # Don't replace the Waived scope.
            waived_labels = [f'{defs.CKI_KERNEL_RT_PREFIX}',
                             f'{defs.CKI_KERNEL_AUTOMOTIVE_PREFIX}']

            labels_to_add = discard_waived_labels(mr_labels,
                                                  labels_to_add,
                                                  waived_labels)

            add_labels(namespace, mr_id, labels_to_add)
        return
    # or if the MR doesn't have CKI labels add ::Missing ones.
    if labels_to_add := check_for_missing_labels(mr_labels, branch.pipelines):
        LOGGER.info('MR is missing one or more CKI labels.')
        add_labels(namespace, mr_id, labels_to_add)
        return
    LOGGER.info('No CKI label changes to make.')
    return


def process_note_event(msg, projects, graphql, **_):
    """Process a note message."""
    if not common.force_webhook_evaluation(msg.payload['object_attributes']['note'],
                                           ['cki', 'ckihook', 'pipeline']):
        LOGGER.info('Note event did not request evaluation, ignoring.')
        return
    mr_id = msg.payload['merge_request']['iid']
    namespace = msg.payload['project']['path_with_namespace']
    branch = projects.get_target_branch(msg.payload['project']['id'],
                                        msg.payload['merge_request']['target_branch'])
    LOGGER.info('Processing note event for %s!%s.', namespace, mr_id)
    if labels_to_add := compute_new_labels(graphql, namespace, mr_id, branch.pipelines):
        add_labels(namespace, mr_id, labels_to_add)
        return
    LOGGER.info('No CKI label changes to make.')
    return


def process_pipeline_event(msg, projects, graphql, **_):
    """Process a pipeline message."""
    # This should filter out upstream pipeline events and any downstream pipeline events which are
    # not directly related to an MR.
    if not (mr_url := common.get_pipeline_variable(msg.payload, 'mr_url')) or \
       ((retrigger := common.get_pipeline_variable(msg.payload, 'retrigger')) and
           misc.strtobool(retrigger)):
        LOGGER.info('Event did not contain the expected variables, ignoring')
        return

    branch = projects.get_target_branch(common.get_pipeline_variable(msg.payload, 'mr_project_id'),
                                        common.get_pipeline_variable(msg.payload, 'branch'))

    namespace, mr_id = parse_mr_url(mr_url)
    pipeline_name = common.get_pipeline_variable(msg.payload, 'trigger_job_name')
    LOGGER.info('Processing pipeline event (id: %s, name: %s) for %s!%s.',
                msg.payload['object_attributes']['id'], pipeline_name, namespace, mr_id)

    pipeline_data = fetch_cki(graphql, namespace, mr_id)
    pipeline = next((pipe for pipe in map_pipeline_query_results(pipeline_data)
                     if pipe.name == pipeline_name), None)
    if pipeline:
        if pipeline.type not in branch.pipelines:
            LOGGER.info("Ignoring pipeline event because %s is not expected on branch '%s'.",
                        pipeline.type, branch.name)
            return
        if pipeline.label:
            LOGGER.info('Setting label %s.', pipeline.label)
            add_labels(namespace, mr_id, [pipeline.label])
            return
    LOGGER.info('No label to set for this pipeline.')
    return


def process_push_event(msg, projects, graphql, **_):
    """Process a push event message."""
    namespace = msg.payload['project']['path_with_namespace']
    branch_name = msg.payload['ref'].rsplit('/', 1)[-1]
    LOGGER.info('Processing push event for %s on branch %s.', namespace, branch_name)
    if not (branch := projects.get_target_branch(msg.payload['project']['id'], branch_name)):
        LOGGER.info('Ignoring push to unrecognized branch.')
        return
    if not branch.name.endswith('-rt'):
        LOGGER.info('Ignoring push to non-rt branch.')
        return
    retrigger_failed_pipelines(graphql, namespace, branch)


WEBHOOKS = {
    'merge_request': process_mr_event,
    'note': process_note_event,
    'pipeline': process_pipeline_event,
    'push': process_push_event
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('CKIHOOK')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, get_gl_instance=False, get_graphql_instance=True)


if __name__ == "__main__":
    main(sys.argv[1:])
