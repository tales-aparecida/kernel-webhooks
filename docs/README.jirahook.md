# Jirahook Webhook

## Purpose

This webhook verifies that a Merge Request's description and commits all reference valid JIRA Issues.

## Reporting

- Label prefix: `JIRA::`
- Comment header: **JIRA Hook Readiness Report**

The webhook reports the overall result of the check by applying a scoped label
to the MR with the prefix `JIRA::`.

The hook will leave a comment on the MR with the details of the check. The
header of the comment is **JIRA Hook Readiness Report**. This comment will be edited
with updated information every time the hook runs. Refer to the timestamp at the
end of the comment to see when the hook last evaluated the MR.

## Triggering

To trigger reevaluation of an MR by the jirahook webhook either remove any
existing `JIRA::` scoped label or a leave a comment with one of the following:

- `request-jira-evaluation`
- `request-jirahook-evaluation`

Alternatively, to retrigger all the webhooks at the same time leave a note in
the MR with `request-evaluation`.

##  Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.jirahook \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

In addition, this webhook requires two environment variables to be set:

- JIRA_TOKEN_AUTH: a [JIRA Personal Access Token](https://issues.redhat.com/secure/ViewProfile.jspa?selectedTab=com.atlassian.pats.pats-plugin:jira-user-personal-access-tokens)
- COMMIT_POLICY_URL: the full URL of the dist-git commit policy file
